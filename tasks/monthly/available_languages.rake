namespace :monthly do
  desc 'languages del_flg = 0'
  task :available_languages => :connect_test do
    # SELECT * FROM languages WHERE del_flg = 0
    TEST_DB[:languages].where(:del_flg => 0).each do |row|
      puts row.values_at(:id, :name, :del_flg).join(',')
    end
# UPDATE
   res = TEST_DB[:languages].where(:del_flg=>1).update(:del_flg=>0)
   #=> 更新したレコード数
   puts "update result : #{res}"   
# INSERT
    res = TEST_DB[:languages].insert(:id=>10, :name=>'c++', :price=>10000)
    puts "insert result : #{res}"
   #=> 挿入したレコードに AUTO_INCREMENT があればその値。無ければ 0

  end #task
end #namespace
