namespace :monthly do
  task :connect_test => :logging do
    TEST_DB = Sequel.mysql2(CONTEXT['databases']['test_db'])
    TEST_DB.loggers << CONTEXT.logger('test')
    class << TEST_DB
      def literal(s)
        Sequel::LiteralString.new(s.to_s)
      end
    end
  end
end
