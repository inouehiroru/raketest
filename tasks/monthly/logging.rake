namespace :monthly do
  task :logging => :configuration do
    raked_at = Time.now.strftime('%Y-%m-%d_%H%M%S')
    log_dir  = CONTEXT['log']['dir']
    # ログ出力ディレクトリが存在しなければ作成しておく
    FileUtils.mkdir_p(log_dir) unless FileTest.directory?(log_dir)
    # CONTEXTへlogger生成メソッドを追加する
    CONTEXT.instance_eval do
      self.class.send :define_method, :logger do |name, level=Logger::INFO|
        logger = Logger.new(File.join(log_dir,
                                      [raked_at, name, 'log'].join('.')))
        logger.level = level
        logger
      end
    end
  end
end
